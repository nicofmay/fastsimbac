#include<iostream>
#include<math.h>
#include<time.h>
#include<sstream>
#include "simulator.h"
#include<string>

using namespace std;

Interval::Interval(bool root, int nLeaves){
	this->p = std::numeric_limits<double>::infinity();
	if(root){
		this->n = nLeaves;
	}else{
		this->n = 0;
	}
}

Interval::Interval(int n, double p){
	this->p = p;
	this->n = n;
}

Interval::~Interval(){	
}

ChrPosition::ChrPosition(unsigned long int iGraphIteration,
double position){
    this->iGraphIteration = iGraphIteration;
    this->position = position;
}

PtrRefCountable::PtrRefCountable(){
    references = 0;
}

PtrRefCountable::~PtrRefCountable(){
//    cout<<"Parent destructor\n";
}

Mutation::Mutation(double dLocation,double dFreq){
    this->dLocation = dLocation;
    this->dFreq = dFreq;
    this->bPrintOutput = false;
}

Mutation::~Mutation(){
//    cout<<"Mutation destructor\n";
}

GeneConversion::GeneConversion(double dEndPos):
PtrRefCountable(){
    this ->dEndPos = dEndPos;
}

GeneConversion::~GeneConversion(){
    #ifdef DIAG
//    cout<<"Gene conversion destructor\n";
    #endif
}

HotSpotBin::HotSpotBin(double dStart, double dEnd,double dRate){
        this->dStart = dStart;
        this->dEnd = dEnd;
        this->dRate = dRate;
}

AlleleFreqBin::AlleleFreqBin(double dStart, double dEnd, double dFreq){
    this->dStart = dStart;
    this->dEnd = dEnd;
    this->dFreq = dFreq;
    this->iObservedCounts = 0;
}

AlleleFreqBin::~AlleleFreqBin(){
    #ifdef DIAG
//    cout<<"AlleleFreqBin destructor\n";
    #endif
}


Population::Population(){
    setChrSampled(0);
    setPopSize(1);
    dGrowthAlpha=0;
    dLastTime=0;
}


void Population::setLastTime(double time){
    this->dLastTime = time;
    #ifdef DIAG
    if (time<0) throw "setLastTime, negative time";
    #endif
}

double Population::getLastTime(){
    return this->dLastTime;
}

void Population::setGrowthAlpha(double alpha){
    this->dGrowthAlpha = alpha;
    #ifdef DIAG
    if (this->dGrowthAlpha<0) throw "setGrowthAlpha, negative alpha";
    #endif
}

double Population::getGrowthAlpha(){
    return this->dGrowthAlpha;
}

void Population::setChrSampled(int iChrSampled){
    this->iChrSampled = iChrSampled;
    #ifdef DIAG
    if (this->iChrSampled<0) throw "setChrSampled, negative chrs";
    #endif
}

void Population::setPopSize(double dPopSize){
    this->dPopSize = dPopSize;
    #ifdef DIAG
    if (this->dPopSize<0) throw "setPopSize, negative pop";
    #endif
}

Edge::Edge(NodePtr & topNode,NodePtr & bottomNode, bool root, int nLeaves):
PtrRefCountable(){
    this->dLength = topNode->getHeight() - bottomNode->getHeight();
    this->topNode = topNode;
    this->getBottomNodeRef() = bottomNode;
    this->bDeleted = false;
    this->bInQueue = false;
    this->bInCurrentTree = false;
    //MRCAdataPtr & ancMat=new MRCAdata;
    this->ancMat = MRCAdataPtr(new MRCAdata);
    IntervalPtr i = IntervalPtr(new Interval(root, nLeaves));
	(* this->ancMat).push_back(i);
}

Edge::Edge(NodePtr & topNode,NodePtr & bottomNode, MRCAdataPtr ancMat2):
PtrRefCountable(){
    this->dLength = topNode->getHeight() - bottomNode->getHeight();
    this->topNode = topNode;
    this->getBottomNodeRef() = bottomNode;
    this->bDeleted = false;
    this->bInQueue = false;
    this->bInCurrentTree = false;
    this->ancMat = MRCAdataPtr(new MRCAdata);
    MRCAdataCopy(ancMat2, this->ancMat);
    //MRCAdata * ancMat2=new MRCAdata;
    //MRCAdataCopy(ancMat, ancMat2);
    //this->ancMat = *ancMat2;
}

Edge::Edge(NodePtr & topNode,NodePtr & bottomNode):
PtrRefCountable(){
    this->dLength = topNode->getHeight() - bottomNode->getHeight();
    this->topNode = topNode;
    this->getBottomNodeRef() = bottomNode;
    this->bDeleted = false;
    this->bInQueue = false;
    this->bInCurrentTree = false;
    //MRCAdata * ancMat2=new MRCAdata;
    this->ancMat = MRCAdataPtr(new MRCAdata);
}


Edge::~Edge(){
	(this->ancMat).reset();
    //delete this->ancMat;
    //(this->ancMat).clear();
}

void Edge::setBottomNode(NodePtr & bottomNode){
    #ifdef DIAG
    if (topNode->getHeight()<bottomNode->getHeight())
    throw "Error in modifying edge.  Top node must be higher or equal";
    #endif
    this->dLength = topNode->getHeight() - bottomNode->getHeight();
    this->bottomNode = bottomNode;
}

const char* Node::getTypeStr(){
    switch(iType){
    case COAL:
        return "coal";
    case XOVER:
        return "xover";
    case MIGRATION:
        return "migr";
    case SAMPLE:
        return "sampl";
    case QUERY:
        return "query";
    }
    return "undef";
}

void Node::addNewEdge(EdgeLocation iLocation, EdgePtr & newEdge){
    switch (iLocation){
        case Node::TOP_EDGE:
            if (topEdgeSize) this->topEdge2 = newEdge;
            else this->topEdge1 = newEdge;
            ++topEdgeSize;
            break;
        case Node::BOTTOM_EDGE:
            if (bottomEdgeSize) this->bottomEdge2 = newEdge;
            else this->bottomEdge1 = newEdge;
            ++bottomEdgeSize;
            break;
        break;
    }
    #ifdef DIAG
    switch(iType){
        case Node::COAL:
            if (bottomEdgeSize>2||
            topEdgeSize>1)
                throw "Coal node has too many edges\n";
            break;
        case Node::XOVER:
            if (topEdgeSize>2||
            bottomEdgeSize>1)
                throw "Xover node has too many edges\n";
            break;
        case Node::MIGRATION:
            if (bottomEdgeSize>1||
            topEdgeSize>1)
                throw "Migration node has too many edges\n";
            break;
        default:
            break;
    }
    #endif
}

void Node::replaceOldWithNewEdge(EdgeLocation iLocation,
EdgePtr & oldEdge,EdgePtr & newEdge){
    bool found = false;
    unsigned int i=0;
    if (iLocation==Node::TOP_EDGE){
        while(!found && i<this->topEdgeSize){
            WeakEdgePtr & topEdge = i?topEdge2:topEdge1;
            if (topEdge.lock()==oldEdge){
                topEdge=WeakEdgePtr(newEdge);
                found = true;
            }else{
                ++i;
            }
        }
        #ifdef DIAG
        if (!found) throw "Can't find top edge in replace edge";
        #endif
    }else if (iLocation==Node::BOTTOM_EDGE){
        while(!found && i<this->bottomEdgeSize){
            WeakEdgePtr & bottomEdge = i?bottomEdge2:bottomEdge1;
            if (bottomEdge.lock()==oldEdge){
                bottomEdge = WeakEdgePtr(newEdge);
                found = true;
            }else{
                ++i;
            }
        }
        if (!found) throw "Can't find bottom edge in replace edge";
    }
}

Node::Node(NodeType iType,short int iPopulation,double dHeight):
PtrRefCountable(){
    this->iType = iType;
    this->iPopulation = iPopulation;
    this->dHeight = dHeight;
    this->bEventDefined = false;
    this->topEdgeSize=0;
    this->bottomEdgeSize=0;
    this->bDeleted = false;
    this->expireDate = std::numeric_limits<double>::infinity();
    #ifdef DIAG
    cerr<<"Graph node at "<<dHeight<<" constructed."<<endl;
    #endif
}

Node::Node(NodeType iType,short int iPopulation,double dHeight, double expireDate):
PtrRefCountable(){
    this->iType = iType;
    this->iPopulation = iPopulation;
    this->dHeight = dHeight;
    this->bEventDefined = false;
    this->topEdgeSize=0;
    this->bottomEdgeSize=0;
    this->bDeleted = false;
    this->expireDate = expireDate;
    #ifdef DIAG
    cerr<<"Graph node at "<<dHeight<<" constructed."<<endl;
    #endif
}

Node::~Node(){
    #ifdef DIAG
//    cerr<<"Graph node at "<<dHeight<<" destructed."<<endl;
    #endif
}

SampleNode::SampleNode(short int iPopulation,int iId):
Node(Node::SAMPLE,iPopulation,0.0){
    this->bAffected = false;
    this->iId = iId;
}


Event::~Event(){
    #ifdef DIAG
//    cerr<<"At time "<<this->dTime<<" Graph event destructor\n";
    #endif
}

Event::Event(EventType iType,double dTime):
PtrRefCountable(){
    #ifdef DIAG
    if (dTime<0) throw "Error in creating event. Time must be positive";
    cerr<<"At time "<<this->dTime<<" Graph event constructor\n";
    #endif
    this->iType = iType;
    this->dTime = dTime;
    this->bMarkedForDelete = false;
    if (dTime>Node::MAX_HEIGHT) throw "Stop here\n";
}


GenericEvent::GenericEvent(EventType iType,double dTime,double dParameterValue):
Event(iType,dTime){
    this->dParameterValue = dParameterValue;
    //cerr<<"destroying generic event at height "<<dTime<<endl;
}

double GenericEvent::getParamValue(){
    return this->dParameterValue;
}

MigrationEvent::MigrationEvent(EventType iType,double dTime,short int iPopMigratedTo,short int iPopMigratedFrom):
Event(iType,dTime){
    this->iPopMigratedTo = iPopMigratedTo;
    this->iPopMigratedFrom = iPopMigratedFrom;
}

MigrationRateEvent::MigrationRateEvent(EventType iType,double dTime,short int iSourcePop,short int iDestPop,double dRate):
Event(iType,dTime){
    this->iSourcePop=iSourcePop;
    this->iDestPop=iDestPop;
    this->dRate=dRate;
}

short int MigrationRateEvent::getSourcePop(){
    return this->iSourcePop;
}

short int MigrationRateEvent::getDestPop(){
    return this->iDestPop;
}

double MigrationRateEvent::getRate(){
    return this->dRate;
}

MigrationRateMatrixEvent::MigrationRateMatrixEvent(EventType iType,double dTime,MatrixDouble dMigrationMatrix):
Event(iType,dTime){
    this->dMigrationMatrix = dMigrationMatrix;
}

MatrixDouble MigrationRateMatrixEvent::getMigrationMatrix(){
    return this->dMigrationMatrix;
}

CoalEvent::CoalEvent(EventType iType,double dTime,short int iPopulation):
Event(iType,dTime){
    this->iPopulation = iPopulation;
}


XoverEvent::XoverEvent(EventType iType,double dTime,
short int iPopulation):Event(iType,dTime){
    this->iPopulation = iPopulation;
}

PopSizeChangeEvent::PopSizeChangeEvent(EventType iType,double dTime,
short int iPopulationIndex,double dPopChangeParam):
Event(iType,dTime){

    this->iPopulationIndex=iPopulationIndex;
    this->dPopChangeParam=dPopChangeParam;
}

short int PopSizeChangeEvent::getPopulationIndex(){
    return iPopulationIndex;
}

double PopSizeChangeEvent::getPopChangeParam(){
    return dPopChangeParam;
}


PopJoinEvent::PopJoinEvent(EventType iType,double dTime, short int iSourcePop,short int iDestPop):
Event(iType,dTime){
    this->iSourcePop = iSourcePop;
    this->iDestPop = iDestPop;
}

short int PopJoinEvent::getSourcePop(){
    return iSourcePop;
}

short int PopJoinEvent::getDestPop(){
    return iDestPop;
}

GraphBuilder::~GraphBuilder(){
    cerr<<"Graphbuilder destructor\n";
    this->pConfig = NULL;
    this->pRandNumGenerator = NULL;

    delete this->pEdgeListInARG;
    delete this->pEdgeVectorByPop;

    delete this->pVectorIndicesToRecycle;
    delete [] pTreeEdgesToCoalesceArray;
    delete this->pEdgeVectorInTree;
    delete [] this->pSampleNodeArray;
    MutationPtrVector::iterator it;
    for(it=pMutationPtrVector->begin();it!=pMutationPtrVector->end();++it){
        delete(*it);
    }
    delete this->pMutationPtrVector;
    delete this->pEventList;
    delete this->pGeneConversionPtrSet;
    delete [] sites;
    delete pChrPositionQueue;
}

GraphBuilder::GraphBuilder(Configuration *pConfig,RandNumGenerator * pRG){
    this->iGraphIteration = 0;
    this->curPos=-std::numeric_limits<double>::infinity();
    this->bIncrementHistory = false;
    this->iTotalTreeEdges = 0;
    this->dArgLength = 0.;
    this->pConfig = pConfig;
    this->pChrPositionQueue = new ChrPositionQueue;
    this->dTrailingGap = pConfig->dBasesToTrack/pConfig->dSeqLength;
    this->bEndGeneConversion = false;
    this->bBeginGeneConversion = false;
    this->dScaledRecombRate = pConfig->dRecombRateRAcrossSites;//*(pConfig->dGeneConvRatio+1.);
    this->dScaledRecombRate2 = pConfig->dRecombRateRAcrossSites2;//*(pConfig->dGeneConvRatio+1.);
    this->dMigrationMatrix = pConfig->dMigrationMatrix;
    this->pRandNumGenerator = pRG;
    this->pEdgeVectorByPop = new EdgePtrVectorByPop;
    this->pVectorIndicesToRecycle = new EdgeIndexQueueByPop;
    this->pEdgeListInARG = new EdgePtrList;
    this->pTreeEdgesToCoalesceArray = new EdgePtr[pConfig->iSampleSize];
    for (int i=0;i<pConfig->iTotalPops;++i){
        this->pEdgeVectorByPop->push_back(EdgePtrVector());
        cerr<<"DEBUG: Size at "<<i<<" is "<<this->pEdgeVectorByPop->at(i).size()<<endl;
        this->pVectorIndicesToRecycle->push_back(EdgeIndexQueue());
    }
    this->pSampleNodeArray = new NodePtr[pConfig->iSampleSize];
    sites = new bool[pConfig->iSampleSize];
    this->pEdgeVectorInTree = new EdgePtrVector;
    this->pMutationPtrVector = new MutationPtrVector;
    this->pEventList = new EventPtrList;
    EventPtrList::iterator it = pConfig->pEventList->begin();
    for (it=pConfig->pEventList->begin();it!=pConfig->pEventList->end();++it){
        this->pEventList->push_back(*it);
    }
    this->pGeneConversionPtrSet = new GeneConversionPtrSet;
    if (pConfig->pAlleleFreqBinPtrSet!=NULL){
      AlleleFreqBinPtrSet::iterator it2;
      for (it2=pConfig->pAlleleFreqBinPtrSet->begin();it2!=pConfig->pAlleleFreqBinPtrSet->end();++it2){
        AlleleFreqBinPtr ptr = *it2;
        ptr->iObservedCounts = 0;
      }
    }
}

void GraphBuilder::checkPopCountIntegrity(PopVector & pPopList,double dTime){
    int iTotalPops = pPopList.size();
    vector<int> iCounts;
    vector <vector <int> > edgeIndices;
    for (int k=0;k<iTotalPops;++k){
        vector<int> temp;
        edgeIndices.push_back(temp);
        iCounts.push_back(0);    }
    for (int k=0;k<iTotalPops;++k){
        EdgePtrVector & pEdgeVector = this->pEdgeVectorByPop->at(k);
        for (unsigned int i=0;i<pEdgeVector.size();++i){
            if (!pEdgeVector[i]->bDeleted && pEdgeVector[i]
            ->getBottomNodeRef()->getHeight()<=dTime &&
            pEdgeVector[i]->getTopNodeRef()->getHeight()>dTime){
                int iPopulation = pEdgeVector[i]->getBottomNodeRef()->getPopulation();
                ++iCounts[iPopulation];
                edgeIndices[iPopulation].push_back(i);
            }
        }
        if (coalescingEdge->getBottomNodeRef()->getPopulation()==k &&
            coalescingEdge->getBottomNodeRef()->getHeight()<=dTime &&
            coalescingEdge->getTopNodeRef()->getHeight()>dTime){
                ++iCounts[k];
        }
        if (originExtension->getBottomNodeRef()->getPopulation()==k &&
            originExtension->getBottomNodeRef()->getHeight()<=dTime &&
            originExtension->getTopNodeRef()->getHeight()>dTime){
                ++iCounts[k];
        }
    }

    for (int j=0;j<iTotalPops;++j){
        if (pPopList[j].getChrSampled()!=iCounts[j]){
            cerr<<"At time: "<<dTime<<endl;
            cerr<<"pop:"<<j<<",size:"<<pPopList[j].getChrSampled()<<endl;
            cerr<<"pop:"<<j<<",found:"<<iCounts[j]<<endl;
            printDataStructures();
            throw "Mismatch in pop counts in CheckPopCountIntegrity" ;
        }
    }
}

void GraphBuilder::insertNodeInRunningEdge(NodePtr & newNode,EdgePtr & tempEdge){

    NodePtr & bottomNode = tempEdge->getBottomNodeRef();
    NodePtr & topNode = tempEdge->getTopNodeRef();
    #ifdef DIAG
    if (newNode->getHeight()<=bottomNode->getHeight() ||
    newNode->getHeight() >= topNode->getHeight()){
           cerr<<"Edge has heights "<<bottomNode->getHeight()<<
        " and "<<topNode->getHeight()<<endl;
        cerr<<"New node has height "<<newNode->getHeight()<<endl;
        throw "Node to insert does not fit within coal edge ";
    }
    #endif

    EdgePtr tempEdgeCopy = tempEdge;

    tempEdge = EdgePtr(new Edge(topNode,newNode, tempEdgeCopy->ancMat));
    newNode->addNewEdge(Node::TOP_EDGE,tempEdge);
    tempEdge->getTopNodeRef()->replaceOldWithNewEdge(Node::BOTTOM_EDGE,tempEdgeCopy,tempEdge);

    EdgePtr newBottomEdge = EdgePtr(new Edge(newNode,bottomNode, tempEdgeCopy->ancMat));
    newNode->addNewEdge(Node::BOTTOM_EDGE,newBottomEdge);
    addEdge(newBottomEdge);
    bottomNode->replaceOldWithNewEdge(Node::TOP_EDGE,tempEdgeCopy,newBottomEdge);
    deleteEdge(tempEdge);
}

void GraphBuilder::mergeEdges(EdgePtr & topEdge,EdgePtr & bottomEdge){
    #ifdef DIAG
    if (topEdge->getBottomNodeRef()->getHeight()!=bottomEdge->getTopNodeRef()->getHeight())
    throw "The top edge and bottom edge are mismatched for the join";
    //if (topEdge->expireDate!=bottomEdge->expireDate)
    //throw "The top edge and bottom edge have mismatched exipireDates for the join";
    #endif
    // mark expired node as deleted
    //if (pConfig->bDebug)cerr<<" called mergeEdges at heights "<<topEdge->getTopNodeRef()->getHeight()<<" "
	//	<<topEdge->getBottomNodeRef()->getHeight()<<" "<<bottomEdge->getTopNodeRef()->getHeight()
	//	<<" "<<bottomEdge->getBottomNodeRef()->getHeight()<<endl;
    bottomEdge->getTopNodeRef()->bDeleted = true;
    NodePtr & bottomNode = bottomEdge->getBottomNodeRef();
    //NodePtr middleNode = bottomEdge->getTopNodeRef();
    //NodePtr middleNode =bottomEdge->getTopNodeRef();
    bottomNode->replaceOldWithNewEdge(Node::TOP_EDGE,bottomEdge,topEdge);
    //middleNode.reset();
    topEdge->setBottomNode(bottomNode);
    deleteEdge(bottomEdge);
    //if (pConfig->bDebug)cerr<<" finished mergeEdges, new top node has heights "<<topEdge->getTopNodeRef()->getHeight()<<" "
    //<<topEdge->getBottomNodeRef()->getHeight()<<endl;
}

void GraphBuilder::insertNodeInEdge(NodePtr & newNode,EdgePtr & selectedEdge){
    NodePtr bottomNodeCopy = selectedEdge->getBottomNodeRef();
    #ifdef DIAG
    NodePtr & topNode = selectedEdge->getTopNodeRef();
    if (newNode->getHeight()<bottomNodeCopy->getHeight() ||
    newNode->getHeight() > topNode->getHeight()){
        cerr<<"Edge has heights "<<bottomNodeCopy->getHeight()<<
        " and "<<topNode->getHeight()<<endl;
        cerr<<"New node has height "<<newNode->getHeight()<<endl;
        throw "Node to insert does not fit within edge";
    }
    #endif
    selectedEdge->setBottomNode(newNode);
    newNode->addNewEdge(Node::TOP_EDGE,selectedEdge);
    EdgePtr newBottomEdge = EdgePtr(new Edge(newNode,bottomNodeCopy, selectedEdge->ancMat));
    addEdge(newBottomEdge);
    bottomNodeCopy->replaceOldWithNewEdge(Node::TOP_EDGE,selectedEdge,newBottomEdge);
    newNode->addNewEdge(Node::BOTTOM_EDGE,newBottomEdge);
}








void GraphBuilder::deleteEdge(EdgePtr & edge){
    if (!edge->bDeleted){
        edge->bDeleted = true;
        //if (pConfig->bDebug){
        //    cerr<<"Deleting edge with hts "<<edge->getBottomNodeRef()->getHeight()<<" and "<<
        //    edge->getTopNodeRef()->getHeight()<<endl;
        //}
    }
}

// Insert into EdgeVector, pop refers to bottom node
void GraphBuilder::addEdge(EdgePtr & edge){
    unsigned int iPopulation = edge->getBottomNodeRef()->getPopulation();
    //if(pConfig->bDebug) cerr<<"DEBUG addEdge: iPopulation: "<<iPopulation<<" and pEdgeVectorByPop size "<<pEdgeVectorByPop->size()<<endl;
    this->pEdgeListInARG->push_back(edge);
    while (iPopulation>=pEdgeVectorByPop->size()){
        this->pEdgeVectorByPop->push_back(EdgePtrVector());
        this->pVectorIndicesToRecycle->push_back(EdgeIndexQueue());
        if(pConfig->bDebug)cerr<<"DEBUG! addEdge: Adding pop "<<iPopulation<<" has edge vector size: "<<pEdgeVectorByPop->size()<<endl;
    }
    if (iPopulation>=pEdgeVectorByPop->size()){
      if(pConfig->bDebug) cerr<<"DEBUG! addEdge: Still not added! iPopulation: "<<iPopulation<<" and pEdgeVectorByPop size "<<pEdgeVectorByPop->size()<<endl;
      throw "Something wrong with while loop";
    }
    
    // Insert into the vector allowing for random access
    EdgePtrVector & pEdgeVector = this->pEdgeVectorByPop->at(iPopulation);
    EdgeIndexQueue & pVectorIndicesToRecycle = this->pVectorIndicesToRecycle->at(iPopulation);
    if (pVectorIndicesToRecycle.empty()){
        pEdgeVector.push_back(edge);
        //if(pConfig->bDebug) cerr<<"DEBUG: pEdgeVector for pop "<<iPopulation<<" is now size: "<<pEdgeVector.size()<<endl;
    }else{
        int iIndex = pVectorIndicesToRecycle.front();
        pVectorIndicesToRecycle.pop();
        pEdgeVector[iIndex] = edge;
    }
}

void GraphBuilder::addEdgeToCurrentTree(EdgePtr & edge){
    edge->bInCurrentTree = true;
    if (iTotalTreeEdges<pEdgeVectorInTree->size()){
        pEdgeVectorInTree->at(iTotalTreeEdges) = edge;
    }else{
        pEdgeVectorInTree->push_back(edge);
    }
    ++iTotalTreeEdges;
}


void GraphBuilder::initializeCurrentTree(){
    // this method does two things, traverses the edges to compute the total length, and clears out the status for
    // in current tree
    dLastTreeLength = 0.0;
    dArgLength = 0.;
    if (iGraphIteration==0){
        EdgePtrList::iterator it;
        for (it=pEdgeListInARG->begin();it!=pEdgeListInARG->end();++it){
            EdgePtr curEdge=*it;
            dLastTreeLength+=curEdge->getLength();
            this->addEdgeToCurrentTree(curEdge);
            curEdge->bInCurrentTree = false;
        }
        dArgLength = dLastTreeLength;
    }else{
        EdgePtrList::iterator it1=pEdgeListInARG->begin();
        while(it1!=pEdgeListInARG->end()){
            EdgePtr curEdge = *it1;
            if (!curEdge->bDeleted){
              dArgLength+=curEdge->getLength();
              if (curEdge->bInCurrentTree){
                dLastTreeLength+=curEdge->getLength();
                curEdge->bInCurrentTree = false;
              }
              ++it1;
            }else{
				//(*it1).reset();
				it1 = pEdgeListInARG->erase(it1);
            }
        }
    }
}


void GraphBuilder::printHaplotypes(){
    unsigned int iTotalSites = pMutationPtrVector->size();
    bool bZeroCellCount=false;
    if (pConfig->bDebug) cerr<<"Total sites: "<<iTotalSites<<endl;
    if (iTotalSites){
        int iReducedSites=iTotalSites;
        if (pConfig->bSNPAscertainment){
            // first see if any expected count exceed actual counts
            bool bSufficientObs=false;
            do{
                bSufficientObs=true;
                AlleleFreqBinPtrSet::iterator it=pConfig->pAlleleFreqBinPtrSet->begin();
                while(bSufficientObs && !bZeroCellCount && it!=pConfig->pAlleleFreqBinPtrSet->end()){
                    AlleleFreqBinPtr bin = *it;
                    int iExpectedCount = static_cast<int>(bin->dFreq * iReducedSites);
                    if (!iExpectedCount && bin->dFreq>0.){
                        bZeroCellCount = true;
                        if (pConfig->bDebug){
                            cerr<<"Setting zero cell count true because expecting a proportion of "<<
                            bin->dFreq<<" but found "<<iExpectedCount<<endl;
                        }
                    }
                    else if (bin->iObservedCounts<iExpectedCount){
                        bSufficientObs = false;
                        --iReducedSites;
                    }else{
                        ++it;
                    }
                }
            }while(!bSufficientObs && !bZeroCellCount);
            if (bZeroCellCount){
                cerr<<"Warning: Some observed SNP counts were zero when they should have been positive.\n"<<
                "No ascertainment correction was applied.\n"<<
                "Try expanding frequency bin sizes and/or increasing mutation rate.\n";
                iReducedSites = 0;
            }else{
                int tally=0;
                for (AlleleFreqBinPtrSet::iterator it=pConfig->pAlleleFreqBinPtrSet->begin();
                it!=pConfig->pAlleleFreqBinPtrSet->end();++it){
                    AlleleFreqBinPtr bin = *it;
                    double dStart = bin->dStart;
                    double dEnd = bin->dEnd;
                    int iExpectedCount = static_cast<int>(bin->dFreq * iReducedSites);

                    tally+=iExpectedCount;
                    if (pConfig->bDebug) {
                        cerr<<"Looking for SNPs in range "<<dStart<<" to "<<dEnd<<endl;
                        cerr<<"iObserved count: "<<bin->iObservedCounts<<endl;
                        cerr<<"Expecting "<<iExpectedCount<<" SNPS."<<endl;
                    }
                    #ifdef DIAG
                    if (bin->iObservedCounts<iExpectedCount){
                        throw "Too many expected counts";
                    }
                    #endif
                    while(iExpectedCount>0){
                        int iRandIndex = static_cast<int>(pRandNumGenerator->unifRV()*iTotalSites);
                        MutationPtr mutation = pMutationPtrVector->at(iRandIndex);
                        if (!mutation->bPrintOutput && mutation->dFreq>=dStart && mutation->dFreq<=dEnd){
                            mutation->bPrintOutput = true;
                            --iExpectedCount;
                        }
                    }
                }
                iReducedSites = tally;
                cerr<<"Total sites reduced from "<<iTotalSites<<" to "<<iReducedSites<<endl;
            }
        }
        if (iReducedSites){
            MutationPtrVector::iterator it;
            // copy to a temporary vector if ascertained
            cout<<TOTALSAMPLES<<FIELD_DELIMITER<<pConfig->iSampleSize<<endl;
            cout<<TOTALSITES<<FIELD_DELIMITER<<iReducedSites<<endl;
            cout<<SNPBEGIN<<endl;
            if (pConfig->bSNPAscertainment && !bZeroCellCount){
                int origIndex=0;
                bool indexPrinted=false;
                for (it = pMutationPtrVector->begin();
                it!=pMutationPtrVector->end();++it){
                    MutationPtr mutation = *it;
                    if (mutation->bPrintOutput){
                        if (indexPrinted) cout<<FIELD_DELIMITER;
                        cout<<origIndex;
                        indexPrinted=true;
                    }
                    ++origIndex;
                }
            }else{
                for(int i=0;i<iReducedSites;++i){
                  if (i) cout<<FIELD_DELIMITER;
                  cout<<i;
                }
            }
            cout<<endl<<SNPEND<<endl;
        }
    }
}


void GraphBuilder::printDataStructures(){
    cerr<<endl<<"*** Begin printing structures ***"<<endl;

    double trueLen = 0.0;
    cerr<<"Full ARG (list of edges)\n";
    trueLen = 0.0;
    for (EdgePtrList::iterator it=pEdgeListInARG->begin();it!=pEdgeListInARG->end();it++){
        EdgePtr curEdge = *it;
        cerr<<"low:ht:"<<curEdge->getBottomNodeRef()->getHeight()<<
        ",type:"<<curEdge->getBottomNodeRef()->getTypeStr()<<
        ",pop:"<<curEdge->getBottomNodeRef()->getPopulation()<<
        ";high:ht:"<<curEdge->getTopNodeRef()->getHeight()<<
        ",type:"<<curEdge->getTopNodeRef()->getTypeStr()<<
        ",pop:"<<curEdge->getTopNodeRef()->getPopulation()<<
        ",ancMat:"<<MRCAdataToString(curEdge->ancMat)<<
        ",del:"<<curEdge->bDeleted<<endl;
        trueLen+=curEdge->getLength();
    }


    cerr<<"Last tree (list of edges)\n";
    trueLen = 0.0;
    EdgePtrVector::iterator it=pEdgeVectorInTree->begin();
    unsigned int count=0;
    while(count<iTotalTreeEdges){
        EdgePtr curEdge = *it;
        cerr<<"low_ht:"<<curEdge->getBottomNodeRef()->getHeight()<<
        ",type:"<<curEdge->getBottomNodeRef()->getTypeStr()<<
        ",pop:"<<curEdge->getBottomNodeRef()->getPopulation()<<
        ";high_ht:"<<curEdge->getTopNodeRef()->getHeight()<<
        ",type:"<<curEdge->getTopNodeRef()->getTypeStr()<<
        ",pop:"<<curEdge->getTopNodeRef()->getPopulation()<<
        ",ancMat:"<<MRCAdataToString( curEdge->ancMat)<<endl;
        trueLen+=curEdge->getLength();
        ++count;
        ++it;
    }
    cerr<<"MRCA: "<<localMRCA->getHeight()<<endl;
    cerr<<"Graph grandMRCA: "<<grandMRCA->getHeight()<<endl;
    cerr<<"*** Done printing structures ***"<<endl;
}

//Nicola: print extended Newick format of the current ARG.
string GraphBuilder::getExtNewickTree(double lastCoalHeight,NodePtr & curNode, EdgePtr & edgeFrom, double curPos){
    ostringstream oss;
    if(false){
		//Nicola: not to be used with migration etc.
		if (curNode->getType()==Node::COAL && curNode->getBottomEdgeSize()<2){
			cerr<<" coal node "<<curNode->getHeight()<<" has 1 child only."<<endl;
			exit(0);
		}
		if (curNode->getType()==Node::XOVER && curNode->getTopEdgeSize()<2){
			cerr<<" recomb node "<<curNode->getHeight()<<" has 1 child only."<<endl;
			exit(0);
		}
		if (curNode->getBottomEdgeSize()==1 && curNode->getTopEdgeSize()==1){
			cerr<<" node "<<curNode->getHeight()<<" has 1 child only and 1 parent only."<<endl;
			exit(0);
		}
	}
    if (curNode->getType()==Node::SAMPLE){
        // print the Extended Newick format with the Sample or gene conversion ID followed by height
        SampleNode * pSampleNode = static_cast<SampleNode *>(curNode.get());
        oss<<pSampleNode->iId<<":0.0";
    }else if (curNode->getTopEdgeSize()>1 && edgeFrom!=curNode->getTopEdgeByIndex(0)){
		oss<<"GC"<<curNode->getHeight()<<":"<<curNode->getHeight();
	}else if (curNode->getTopEdgeSize()>1 && edgeFrom==curNode->getTopEdgeByIndex(0)){
		EdgePtr edgeBottom0 = curNode->getBottomEdgeByIndex(0);
		oss<<"("<<getExtNewickTree(lastCoalHeight, edgeBottom0->getBottomNodeRef(),edgeBottom0, curPos)
        <<"-"<<MRCAdataToString(edgeBottom0->ancMat)<<")GC"<<curNode->getHeight();
        oss<<":"<<curNode->getHeight();
    }else if (curNode->getType()==Node::COAL){
        // Here numbers represent nodes heights not branch lengths.
        if(enclosedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(0)->ancMat) && 
        enclosedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(1)->ancMat)){
			EdgePtr edgeBottom0 = curNode->getBottomEdgeByIndex(0);
			EdgePtr edgeBottom1 = curNode->getBottomEdgeByIndex(1);
            oss<<"("<<getExtNewickTree(curNode->getHeight(), edgeBottom0->getBottomNodeRef(),edgeBottom0, curPos)
            <<"-"<<MRCAdataToString(edgeBottom0->ancMat)<<","<<getExtNewickTree(curNode->getHeight(),
            edgeBottom1->getBottomNodeRef(),edgeBottom1, curPos)<<"-"<<MRCAdataToString(edgeBottom1->ancMat)<<")";
            oss<<":"<<curNode->getHeight();
		}else{
            for(int i=0;i<2;++i){
				if(enclosedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(i)->ancMat)){
					EdgePtr edgeBottom = curNode->getBottomEdgeByIndex(i);
                    oss<<"("<<getExtNewickTree(lastCoalHeight, edgeBottom->getBottomNodeRef(),edgeBottom, curPos)
                    <<"-"<<MRCAdataToString(edgeBottom->ancMat)<<")";
                    oss<<":"<<curNode->getHeight();
                }
            }
        }
    }else {
		EdgePtr edgeBottom = curNode->getBottomEdgeByIndex(0);
		oss<<"("<<getExtNewickTree(lastCoalHeight, edgeBottom->getBottomNodeRef(), edgeBottom, curPos)
        <<"-"<<MRCAdataToString(edgeBottom->ancMat)<<")";
        oss<<":"<<curNode->getHeight();
    }
    return oss.str();
}


string GraphBuilder::getNewickTree(double lastCoalHeight,NodePtr & curNode, double curPos){
    ostringstream oss;
    //cerr<<curNode->getHeight()<<endl;
    if (curNode->getType()==Node::SAMPLE){
        // print the Newick format with the Sample ID followed by length
        SampleNode * pSampleNode = static_cast<SampleNode *>(curNode.get());
        oss<<pSampleNode->iId<<":"<<lastCoalHeight-curNode->getHeight();
    }else if (curNode->getType()==Node::COAL){
        // if the two lower branches are in the current tree, then print the
        // Newick format, and recursively repeat
        if(containedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(0)->ancMat) && 
        containedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(1)->ancMat)){
            oss<<"("<<getNewickTree(curNode->getHeight(),curNode->getBottomEdgeByIndex(0)->getBottomNodeRef(), curPos)
            <<","<<getNewickTree(curNode->getHeight(),curNode->getBottomEdgeByIndex(1)->getBottomNodeRef(), curPos)<<")";
            // if we are at the MRCA we don't need to output the branch length since
            // it is obviously 0 length
            if (curNode!=localMRCA) oss<<":"<<lastCoalHeight-curNode->getHeight();
			
		}else{
            for(int i=0;i<2;++i){
				if(containedInMRCAdata(curPos, pConfig->iSampleSize, curNode->getBottomEdgeByIndex(i)->ancMat)){
                    oss<<getNewickTree(lastCoalHeight,
                    curNode->getBottomEdgeByIndex(i)->getBottomNodeRef(), curPos);
                }
            }
        }
    }else {
        // this is for everything else(e.g. migration, xover nodes),
        // don't print anything, but recursively
        // descend down the graph
        oss<<getNewickTree(lastCoalHeight,
        curNode->getBottomEdgeByIndex(0)->getBottomNodeRef(), curPos);
    }
    return oss.str();
}










//update ancestral material to current position, fast
void updateMRCAdataFast(double curPos, MRCAdataPtr & ancMat){
	MRCAdata::iterator it=ancMat->begin();
	MRCAdata::iterator it2=ancMat->begin();
	while(it!=ancMat->end()) {
		it2++;
		if (it2!=ancMat->end()){
			if ((*it)->p <= curPos){
				//it=ancMat.erase(it);
				it=(* ancMat).erase(it);
			}else return;
		}else return;
	}
	return;
}

//update ancestral material to current position (also removing redundant intervals)
void updateMRCAdata(double curPos, MRCAdataPtr & ancMat){
	MRCAdata::iterator it=ancMat->begin();
	MRCAdata::iterator it2=ancMat->begin();
	while(it!=ancMat->end()) {
		it2++;
		if (it2!=ancMat->end()){
			if ((*it)->p <= curPos){
				it=(* ancMat).erase(it);
			}else if((*it)->n==(*it2)->n){
				it=(* ancMat).erase(it);
			}else it++;
		}else return;
	}
}

//ancestral material contains current position
bool containedInMRCAdata(double curPos, int nLeaves, MRCAdataPtr & ancMat){
	MRCAdata::iterator it=ancMat->begin();
	while(it!=ancMat->end()) {
		if((*it)->p > curPos){
			if ((*it)->n >0 && (*it)->n < nLeaves){
				return true;
			}else return false;
		}
		it++;
	}
	cerr<<" attention, you should not reach the end of containedInMRCAdata"<<endl;
	return false;
}

//ancestral material extends over current position
bool enclosedInMRCAdata(double curPos, int nLeaves, MRCAdataPtr & ancMat){
	//cerr<<" calling enclosedInMRCAdata on "<<MRCAdataToString(ancMat)<<endl;
	MRCAdata::iterator it=ancMat->end();
	if(false){
		//Nicola: testing that all is right
		while(it!=ancMat->end()) {
			if((*it)->n > nLeaves || (*it)->n < 0){
				cerr<<" ATTENTION, ancMat "<<MRCAdataToString(ancMat)<<" in enclose check is wrong!"<<endl;
				exit(0);
			}
			it++;
		}
		it=ancMat->end();
	}
	it--;
	int v=-1;
	while(it!=ancMat->begin()) {
		if((*it)->p > curPos){
			if ((*it)->n >0 && (*it)->n < nLeaves) return true;
			else if ((*it)->n <0 || (*it)->n > nLeaves){
				cerr<<" found nonsense ancMat in enclosedinMRCAdata: "<<MRCAdataToString(ancMat)<<endl;
			}else if(v==-1){
				v=(*it)->n;
			}else if ((*it)->n != v) return true;
		}else return false;
		it--;
	}
	if((*it)->p > curPos){
		if ((*it)->n >0 && (*it)->n < nLeaves) return true;
		else if ((*it)->n <0 || (*it)->n > nLeaves){
			cerr<<" found nonsense ancMat in enclosedinMRCAdata: "<<MRCAdataToString(ancMat)<<endl;
		}else if (v!=-1 && (*it)->n != v) return true;
		else return false;
	}else return false;
	cerr<<" ATTENTION!!!, you should not reach the end of enclosedInMRCAdata"<<endl;
	return false;
}

//returns (into data) data1 after removing interval up to expireD.
void removeIntervalMRCAdata(double expireD, MRCAdataPtr & data1, MRCAdataPtr & data){
	MRCAdata::iterator it1=data1->begin();
	bool bFirst=true;
	double p1=(*it1)->p;
	while(it1!=data1->end()){
		p1=(*it1)->p;
		if(p1>expireD){
			if(bFirst){
				bFirst=false;
				if((*it1)->n!=0){
					IntervalPtr i = IntervalPtr(new Interval(0, expireD));
					(* data).push_back(i);
				}
			}
			IntervalPtr i2 = IntervalPtr(new Interval((*it1)->n, p1));
			(* data).push_back(i2);
		}
		it1++;
	}
}

//returns into data the intersection of data1 with interval up to expireD.
void intersectionIntervalMRCAdata(double expireD, MRCAdataPtr & data1, MRCAdataPtr & data){
	MRCAdata::iterator it1=data1->begin();
	double p1=(*it1)->p;
	int lastI;
	while(it1!=data1->end()){
		p1=(*it1)->p;
		if(p1<expireD){
			IntervalPtr i2 = IntervalPtr(new Interval((*it1)->n, p1));
			(* data).push_back(i2);
		}else if(p1>=expireD){
			lastI=(*it1)->n;
			IntervalPtr i2 = IntervalPtr(new Interval(lastI, expireD));
			(* data).push_back(i2);
			break;
		}
		it1++;
	}
	if(lastI!=0){
		IntervalPtr i = IntervalPtr(new Interval(0, std::numeric_limits<double>::infinity()));
		(* data).push_back(i);
	}else{
		((* data).back())->p=std::numeric_limits<double>::infinity();
	}
}

//make each value negative
void negativeMRCAdata(MRCAdataPtr & data1){
	MRCAdata::iterator it1=( data1)->begin();
	while(it1!=data1->end()){
		(*it1)->n = - ((*it1)->n);
		it1++;
	}
}

//sum up two MRCAdata into data.
void sumMRCAdata(MRCAdataPtr & data1, MRCAdataPtr & data2 , MRCAdataPtr & data){
	MRCAdata::iterator it1=data1->begin();
	MRCAdata::iterator it2=data2->begin();
	//MRCAdata::iterator dataEnd=data->end();
	double p1=(*it1)->p, p2=(*it2)->p;
	int lastI=-1;
	int newN;
	while(p1!=std::numeric_limits<double>::infinity() || p2!=std::numeric_limits<double>::infinity()){
		newN=(*it1)->n + (*it2)->n;
		if (p1<p2) {
			if(newN != lastI){
				IntervalPtr i = IntervalPtr(new Interval(newN, p1));
				(* data).push_back(i);
			}else{
				((* data).back())->p=p1;
				//dataEnd--;
				//(*dataEnd)->p=p1;
				//dataEnd++;
			}
			it1++;
			p1=(*it1)->p;
		}else if (p2<p1){
			if(newN != lastI){
				IntervalPtr i = IntervalPtr(new Interval(newN, p2));
				(* data).push_back(i);
			}else{
				((* data).back())->p=p2;
				//dataEnd--;
				//(*dataEnd)->p=p2;
				//dataEnd++;
			}
			it2++;
			p2=(*it2)->p;
		}else if (p1==p2){
			if(newN != lastI){
				IntervalPtr i = IntervalPtr(new Interval(newN, p1));
				(* data).push_back(i);
			}else{
				((* data).back())->p=p1;
				//dataEnd--;
				//(*dataEnd)->p=p1;
				//dataEnd++;
			}
			it1++;
			it2++;
			p1=(*it1)->p;
			p2=(*it2)->p;
		}
		lastI=newN;
	}
	newN=(*it1)->n + (*it2)->n;
	if(newN != lastI){
		IntervalPtr i = IntervalPtr(new Interval((*it1)->n + (*it2)->n, (*it1)->p));
		(* data).push_back(i);
	}else{
		((* data).back())->p=p1;
		//dataEnd--;
		//(*dataEnd)->p=p1;
		//dataEnd++;
	}
}

//make string to show an MRCAdata.
std::string MRCAdataToString(MRCAdataPtr & data1){
	std::ostringstream ss;
	ss<<"[";
	MRCAdata::iterator it1=(data1)->begin();
	while(it1!=(data1)->end()){
		//* i1= * it1;
		ss<<"("<<(*it1)->n<<","<<(*it1)->p<<") ";
		it1++;
	}
	ss<<"]";
	return ss.str();	
}

//make copy of MRCAdata
MRCAdataPtr MRCAdataCopy(MRCAdataPtr & data1){
	MRCAdataPtr data=MRCAdataPtr(new MRCAdata);
	MRCAdata::iterator it1=(data1)->begin();
	while(it1!=(data1)->end()){
		IntervalPtr i = IntervalPtr(new Interval((*it1)->n, (*it1)->p));
		(* data).push_back(i);
		it1++;
	}
	return data;
}

//make copy of data1 into data
void MRCAdataCopy(MRCAdataPtr & data1, MRCAdataPtr & data){
	MRCAdata::iterator it1=(data1)->begin();
	while(it1!=(data1)->end()){
		IntervalPtr i = IntervalPtr(new Interval((*it1)->n, (*it1)->p));
		(* data).push_back(i);
		it1++;
	}
}

//create new MRCAdata
void createMRCAdata(MRCAdataPtr & data, int n){
	IntervalPtr i = IntervalPtr(new Interval(n, std::numeric_limits<double>::infinity()));
	(* data).push_back(i);
}



//Nicola: Iterate over edges above the current one to update ancestral material to add (possibly something negative) . 
//To be called on recombining branch and on sibling (upward) one.
void GraphBuilder::addMRCAUp(EdgePtr & selectedEdge, MRCAdataPtr & toAdd, double curPos){
	MRCAdataPtr sum = MRCAdataPtr(new MRCAdata);
	sumMRCAdata(toAdd, selectedEdge->ancMat, sum);
	//updateMRCAdata(curPos, sum);
	//delete selectedEdge->ancMat;
	(selectedEdge->ancMat).reset();
	selectedEdge->ancMat= sum;
	//now iterate above
	if(selectedEdge->getTopNodeRef()->getTopEdgeSize()==1){
		EdgePtr topEdge0=selectedEdge->getTopNodeRef()->getTopEdgeByIndex(0);
		EdgePtr & newTopEdge0=topEdge0;
		addMRCAUp(newTopEdge0, toAdd, curPos);
		updateMRCAdata(curPos, newTopEdge0->ancMat);
	}else if(selectedEdge->getTopNodeRef()->getTopEdgeSize()==2){
		EdgePtr topEdge0=selectedEdge->getTopNodeRef()->getTopEdgeByIndex(0);
		EdgePtr & newTopEdge0=topEdge0;
		EdgePtr topEdge1=selectedEdge->getTopNodeRef()->getTopEdgeByIndex(1);
		EdgePtr & newTopEdge1=topEdge1;
		MRCAdataPtr reduced0 = MRCAdataPtr(new MRCAdata);
		MRCAdataPtr reduced1 = MRCAdataPtr(new MRCAdata);
		removeIntervalMRCAdata(selectedEdge->getTopNodeRef()->expireDate, toAdd, reduced0);
		intersectionIntervalMRCAdata(selectedEdge->getTopNodeRef()->expireDate, toAdd, reduced1);
		if((*(reduced0->begin()))->n>0 || (*(reduced0->begin()))->p<std::numeric_limits<double>::infinity()) 
		addMRCAUp(newTopEdge0, reduced0, curPos);
		//updateMRCAdata(curPos, newTopEdge0->ancMat);
		if((*(reduced1->begin()))->n>0 || (*(reduced1->begin()))->p<std::numeric_limits<double>::infinity()) 
		addMRCAUp(newTopEdge1, reduced1, curPos);
		//updateMRCAdata(curPos, newTopEdge1->ancMat);
		//delete reduced0;
		//delete reduced1;
		reduced0.reset();
		reduced1.reset();
	}
}

//Nicola: Iterate over nodes to update MRCAdata in a tree, to be called on grandMRCA.
void GraphBuilder::updateMRCAtree(NodePtr & node, double curPos, EdgePtr & fromEdge){
	if(node->getTopEdgeSize()==2){
		if( node->getTopEdgeByIndex(0)==fromEdge){//node->getTopEdgeSize()>0 &&
			EdgePtr edge=node->getBottomEdgeByIndex(0);
			updateMRCAdataFast(curPos, edge->ancMat);
			updateMRCAtree(edge->getBottomNodeRef(), curPos, edge);
		}
	}
	else{
		for(int i=0;i<node->getBottomEdgeSize();++i){
			EdgePtr edge=node->getBottomEdgeByIndex(i);
			updateMRCAdataFast(curPos, edge->ancMat);
			updateMRCAtree(edge->getBottomNodeRef(), curPos, edge);
		}
	}
}

//Nicola: when creating the initial tree, define ancMat for an edge. Assume input pointer to point to empty MRCAdata.
void GraphBuilder::defineMRCA(EdgePtr & selectedEdge, MRCAdataPtr & ancMat){
	//MRCAdata * data1 = new MRCAdata;
	//MRCAdata * data2 = new MRCAdata;
	MRCAdataPtr data1 = NULL;
	MRCAdataPtr data2 = NULL;
	int numEdges=selectedEdge->getBottomNodeRef()->getBottomEdgeSize();
	if (numEdges==0){
		IntervalPtr i = IntervalPtr(new Interval(true, 1));
		(* ancMat).push_back(i);
	}else if (numEdges==1){
		data1= selectedEdge->getBottomNodeRef()->getBottomEdgeByIndex(0)->ancMat;
		MRCAdataCopy(data1, ancMat);
	}else if (numEdges==2){
		data1= (selectedEdge->getBottomNodeRef()->getBottomEdgeByIndex(0)->ancMat);
		data2= (selectedEdge->getBottomNodeRef()->getBottomEdgeByIndex(1)->ancMat);
		sumMRCAdata(data1, data2 , ancMat);
	}
}
